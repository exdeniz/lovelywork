$('.textSizeSmall').click(function(argument) {
    $('.textSize div').removeClass("textSizeItemActive");
    $(this).addClass("textSizeItemActive");
    $('.pagesContent').removeClass().addClass("pagesContent").addClass("pagesContentSmall");
});
$('.textSizeMedium').click(function(argument) {
    $('.textSize div').removeClass("textSizeItemActive");
    $(this).addClass("textSizeItemActive");
    $('.pagesContent').removeClass().addClass("pagesContent").addClass("pagesContentMedium");
});

$('.textSizeBig').click(function(argument) {
    $('.textSize div').removeClass("textSizeItemActive");
    $(this).addClass("textSizeItemActive");
    $('.pagesContent').removeClass().addClass("pagesContent").addClass("pagesContentBig");
});


$(function() {
    $(".buttonLoginJS").on("click", function(e) {
        $('.overlay').addClass("overlayShow");
        $('.login').addClass("loginShow");
    });
    $(".buttonRegJS").on("click", function(e) {
        $('.overlay').addClass("overlayShow");
        $('.registration').addClass("registrationShow");
    });
    $("#regLink").on("click", function(e) {
        $('.login').removeClass("loginShow");
        $('.registration').addClass("registrationShow");
    });

    $('.overlay').on("click", function(e) {
        if ($(e.target).is(".login") === false) {
            $('.overlay').removeClass("overlayShow");
            $('.login').removeClass("loginShow");
            $('.registration').removeClass("registrationShow");
        }
    });
    $("#remeberLink").on("click", function(e) {
        $('.loginBlock').hide();
        $('.rememberBlock').show();
        $('.login').addClass('remember');
    });
    $("#loginFromRegLink").on("click", function(e) {
        $('.registration').removeClass("registrationShow");
        $('.login').addClass("loginShow");
    });

    $("#loginLink").on("click", function(e) {
        $('.rememberBlock').hide();
        $('.loginBlock').show();
        $('.login').removeClass('remember');
    });


    $("#buttonMain").on("click", function(e) {
        $('.overlay').removeClass("overlayShow");
        $('.login').removeClass("loginShow");
        $('.registration').removeClass("registrationShow");
    });
    $("#buttonRegistrationSubmit").on("click", function(e) {
        $('.registrationBlock').hide();
        $('.registrationFormFooter').hide();
        $('.registrationCompliteBlock').show();
        $('.registration').addClass('registrationComplite');
    });

});






var toValidate = $('#emailLogin, #passwordLogin'),
    valid = false;

toValidate.keyup(function() {
    if ($(this).val().length > 0) {
        $(this).data('valid', true);
    } else {
        $(this).data('valid', false);
    }
    toValidate.each(function() {
        if ($(this).data('valid') == true) {
            valid = true;
        } else {
            valid = false;
        }
    });
    if (valid === true) {
        $('#buttonLoginSubmit').removeClass('buttonBlueDisable');
    } else {
        $('#buttonLoginSubmit').addClass('buttonBlueDisable');

    }
});



jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
});
$(".registrationForm").validate({
    success: function(label) {
        label.parents().children(".inputIconComplete").addClass('inputIconCompleteShow');
    },
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true
        },
        first: {
            required: true
        },
        last: {
            required: true
        },
        middle: {
            required: false,
            minlength: 1
        }

    },
    messages: {
        email: {
            required: "Это поле должно быть заполнено",
            email: "Неверный адрес эл. почты"
        }
    }
});

$('#emailPhone').inputmask({
    mask: "+7 (999) 999-9999",
    oncomplete: function() {
        $('.inputIconLoad').removeClass('inputIconLoadShow');
        $('#buttonRememberSubmit').removeClass('buttonBlueDisable');
        $('#emailPhone').parents().children(".inputIconComplete").addClass('inputIconCompleteShow');
    },

    onKeyDown: function() {
        if ($('#emailPhone').inputmask("isComplete")) {
            $('.inputIconLoad').removeClass('inputIconLoadShow');
        } else {
            $('.inputIconLoad').addClass('inputIconLoadShow');
        }
    },
});

$("#registrationPhone", "body")
    .inputmask({
        mask: "+7 (999) 999-9999",
        oncomplete: function() {
            $('.inputIconLoad').removeClass('inputIconLoadShow');
            $('#buttonSMSSubmit').removeClass('buttonBlueDisable');
            $("#registrationPhone").parents().children(".inputIconComplete").addClass('inputIconCompleteShow');
            $('#buttonSMSSubmit').removeAttr("disabled");

        },

        onKeyDown: function() {
            if ($('#registrationPhone').inputmask("isComplete")) {
                $('.inputIconLoad').removeClass('inputIconLoadShow');
            } else {
                $('.inputIconLoad').addClass('inputIconLoadShow');
            }
        },
    })
    .bind("blur", function() {
        // force revalidate on blur.

        var frm = $(this).parents("form");
        // if form has a validator
        if ($.data(frm[0], 'validator')) {
            var validator = $(this).parents("form").validate();
            validator.settings.onfocusout.apply(validator, [this]);
        }
    });

$('#buttonSMSSubmit').click(function() {
    $(this).text('Высылаю СМС с кодом');
    $(this).append('<img src="./icons/spin.svg"></img>');
    $('#buttonSMSSubmitLoad').show();
    if ($('.registrationForm').valid()) { // checks form for validity
        $('#buttonRegistrationSubmit').prop('disabled', false);
        $('#buttonRegistrationSubmit').removeClass('buttonBlueDisable');
    } else {
        $('#buttonRegistrationSubmit').prop('disabled', 'disabled');
        $('#buttonRegistrationSubmit').addClass('buttonBlueDisable');
    }
});

