var gulp = require('gulp'),
    newer = require('gulp-newer'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

gulp.task('fonts', function() {
    gulp.src('./assets/fonts/**/*')
        .pipe(newer('./public/fonts'))
        .pipe(gulp.dest('./public/fonts'))
        .pipe(reload({
            stream: true,
        }));

});


// gulp.task('fontgen', function() {
//     gulp.src('./assets/fonts_source/*.otf')
//         .pipe(fontgen({
//             dest: './public/fonts/'
//         }))
//         .pipe(gulp.dest('./public/fonts/'));
// });