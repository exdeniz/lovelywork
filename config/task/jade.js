var gulp = require('gulp'),
    jade = require('gulp-jade'),
    browserSync = require('browser-sync'),
    cssBase64 = require('gulp-css-base64'),
    reload = browserSync.reload;

// Собираем html из Jade
gulp.task('jadehtml', function() {
    gulp.src(['./assets/template/*.jade', '!./assets/template/_*.jade'])
        .pipe(jade({
            pretty: true
        })) // Собираем Jade только в папке ./assets/template/ исключая файлы с _*
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        .pipe(gulp.dest('./public/')) // Записываем собранные файлы
        .pipe(reload({
            stream: true
        }));
});


gulp.task('jade', function() {
    gulp.src(['./assets/template/*.jade', '!./assets/template/_*.jade'])
        // .pipe(jade({
        //     pretty: true
        // }))  // Собираем Jade только в папке ./assets/template/ исключая файлы с _*
        // .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        // .pipe(gulp.dest('./public/')) // Записываем собранные файлы
        .pipe(browserSync.reload({
            stream: true
        }));
});